<?php 

	require 'check.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ajouter - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>
			<h1>INVENTAIRE RT</h1>

			<p>
				Remplissez le formulaire suivant pour ajouter le matériel à la base de données
			</p>

			<form action="ajouter_traitement.php" method="POST">

				<?php
					echo "<br>";

					//Champ catégorie
					echo "Catégorie : <SELECT name=\"categorie\">
									<OPTION> Switch
									<OPTION> Routeur
									<OPTION> Connectique
									<OPTION> PC fixe
									<OPTION> PC portable
								</SELECT>";
					echo "<br>";

					//Champ nom
					echo "Nom : <input type=\"text\" name=\"description\">";
					echo "<br>";

					//Champ statut
					echo "Statut : <SELECT name=\"statut\">
									<OPTION> Disponible
									<OPTION> En réparation
									</SELECT>";
					echo "<br>";

					//Champ salle
					echo "Salle : <SELECT name=\"salle\">
									<OPTION> PROJ-DOC
									<OPTION> CABL-RES
									<OPTION> INFO-PROG
									<OPTION> BUREAU-ADMIN
									<OPTION> ELEC-TEL
									<OPTION> GENIE-INFO
									</SELECT>";
					echo "<br>";
				?>

				<br>
				<input type="submit" value="Ajouter" name="valider">

			</form>

		</center>
	</body>
</html>

<?php
	//Script de déconnexion effaçant la session précédemment ouverte
	
	session_start();
	session_unset();
	session_destroy();

	//Renvoi à la page de login
	header("Location: login.php");

?>

<?php

	require 'check.php';

	$id = $_POST['id'];
	$mdp = $_POST['mdp'];

	require '../bdd/connect.php';//Connexion à la bdd

	//Vérification de la validité de l'identifiant et de son mot de passe associé
	$requete = "SELECT id_util, nom_util, prenom_util, classe_util, mdp_util, role_util 
				FROM Utilisateur 
				WHERE id_util = :idConnexion AND mdp_util = :mdpConnexion";

	$reponse = $connexion->prepare($requete);
	$reponse->execute(['idConnexion' => $id, 'mdpConnexion' => $mdp]);


	//Recupère toutes les données sur l'utilisateur	
	$tabDonnees = $reponse->fetch();


	if($mdp == $tabDonnees['mdp_util']) //Renvoie l'utilisateur (etudiant) sur la page dashboard ersion étudiant
	{
		//Enregistre la connexion de l'etudiant
		$_SESSION['id'] = $tabDonnees['id_util'];
		$_SESSION['nom'] = $tabDonnees['nom_util'];
		$_SESSION['prenom'] = $tabDonnees['prenom_util'];
		$_SESSION['classe'] = $tabDonnees['classe_util'];
		$_SESSION['role'] = $tabDonnees['role_util']; //Si l'utilisateur est admin ou etudiant

		header('Location: dashboard.php');
	}
	else //Si les informations entrées sont incorrectes
	{
		header('Location: login.php?auth=err'); //Renvoie à la page de connexion avec un avis d'erreur

	}
?>
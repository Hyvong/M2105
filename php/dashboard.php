<?php 
	//Permet d'accéder aux variables de session
	session_start();

	//Renvoie au tableau de reservation si la session n'a pas été fermée
	if(!isset($_SESSION['id']))
	{
		header('Location: login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Liste - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>

			<h1> INVENTAIRE RT </h1>

			<!-- Déconnexion -->
			<a href="logout.php"> Se déconnecter </a>

			<?php
				require '../bdd/connect.php';//Connexion à la bdd

				echo "Bonjour ".$_SESSION['prenom']." ".$_SESSION['nom'];
				echo "<br><br>";
				echo "Tableau des réservations";

				$requete = "SELECT id_mat, dscpt_mat, salle_mat, statut_mat FROM Materiel";
				$reponse = $connexion->query($requete);
			?>

			<table>
				<tr>
					<td>
						ID
					</td>
					<td>
						Description
					</td>
					<td>
						Salle
					</td>
					<td>
						Statut
					</td>
					<?php
						if ($_SESSION['role'] == 1)//Si on est sur un compte administrateur, on affiche la colonne pour supprimer un element
						{
							echo "<td> Suppression </td>";
						}
						else
						{
							echo "<td></td>";
						}
					?>
				</tr>

				<?php

					$i=0;
					foreach ($reponse as $ligne) //Permet d'afficher une ligne sur deux en gris
					{
						$i = 1 - $i;

						if ($i == 1) {
							echo "<tr style=\"background-color: lightgrey;\">";
						}
						else{
							echo "<tr style=\"background-color: white;\">";
						}

						$idMat = $ligne['id_mat'];

						echo "<td>".$ligne['id_mat'].
							"</td><td>".$ligne['dscpt_mat'].
							"</td><td>".$ligne['salle_mat'].
							"</td><td>".$ligne['statut_mat']."</td>";


						//Condition pour ne pas afficher le lien de réservation si l'équiment est indisponible
						if($_SESSION['role'] == 0 && $ligne['statut_mat'] == 'Disponible')
						{
							echo "<td>"."<a href=\"reservation_traitement.php?id=$idMat\">".Réserver."</a>"."</td>";
						}

						elseif ($_SESSION['role'] == 0 && $ligne['statut_mat'] == 'En réparation') {
							echo "<td>"."Contacter l'administrateur"."</td>";
						}

						elseif($_SESSION['role'] == 0)//De même pour le lien de réinitialisation
						{
							$requete = "SELECT id_utilisateur
										FROM Reservation 
										WHERE id_materiel = :idM 
										AND fin_res is null";
							$reponse = $connexion->prepare($requete);
							$reponse->execute(['idM' => $idMat]);
							//Récupère les données demandées via la requête
							$emprunte = $reponse->fetch();

							//Acquisition du nom et prenom de l'utilisateur qui a emprunté
							$requete = "SELECT nom_util, prenom_util
										FROM Utilisateur
										WHERE id_util = :id";
							$reponse = $connexion->prepare($requete);
							$reponse->execute(['id' => $emprunte['id_utilisateur']]);
							$nomprenom = $reponse->fetch();

							//On vérifie si l'utilisateur qui voudrait rendre est bien celui qui a emprunté
							if ($_SESSION['id'] == $emprunte['id_utilisateur'])
							{
								echo "<td>"."<a href=\"reinitialiser.php?id=$idMat\">".Rendre."</a>"."</td>";
							}
							else
							{
								echo "<td>"."Voir avec ".$nomprenom['prenom_util']." ".$nomprenom['nom_util']."</td>";
							}
						}


						//Afffichage du bouton supprimer si l'utilisateur est admin
						if($_SESSION['role'] == 1)
						{
							if($ligne['statut_mat'] != 'Emprunté')
							{
								echo "<td>"."<a href=\"supprimer.php?id=$idMat\">".Supprimer."</a>.</td>";
							}
							else
							{
								echo "<td>"."</td>";
							}
						}

						echo "</tr>\n";
					}
				?>
			</table>

			<?php
				//Possibilité d'ajouter du matériel à la liste si l'utilisateur est admin
				if ($_SESSION['role'] == 1) {
					echo "<p>"."En tant qu'administrateur, vous pouvez ajouter un nouvel élément:"."</p>";
					echo "<a href=\"ajouter.php\">"."Ajouter du matériel"."</a>";
				}
			?>

		</center>
	</body>
</html>
	
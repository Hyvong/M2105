<?php 

	require 'check.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Rendre - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>
			<h1>INVENTAIRE RT</h1>

			<?php

				$idMat = $_GET['id'];


				require '../bdd/connect.php';//Connexion à la bdd

				//Ajout de la date de fin de réservation
				$requete = "UPDATE Reservation
							SET fin_res = CURRENT_TIMESTAMP
							WHERE fin_res is null AND id_materiel = :idM";

				$reponse = $connexion->prepare($requete);
				$reponse->execute(array('idM' => $idMat));

				//Changement du statut de l'équipement
				$requete = "UPDATE Materiel
							SET statut_mat='Disponible'
							WHERE id_mat = :idM";
			
				$reponse = $connexion->prepare($requete);
				$reponse->execute(['idM' => $idMat]);

				echo "Réinitialisation terminée !";
				echo "<br>";

			?>

			<a href="dashboard.php"> Lien vers la liste </a>

		</center>
	</body>
</html>

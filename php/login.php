<?php 
	//Permet d'accéder aux variables de session
	session_start();

	//Renvoie a la page de connexion si l'utilisateur n'est pas déjà connecté
	if(isset($_SESSION['id']))
	{
		header('Location: dashboard.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Connexion - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>

			<h1> INVENTAIRE RT </h1>

			<p>
				Bonjour et bienvenue sur le site d'emprunt du département R&T.<br>
				Connectez-vous pour continuer<br>
			</p>

			<?php
				if($_GET['auth'] == 'err')
				{
					echo "<b> Veuillez entrer des identifiants valides </b><br>";
				}
			?>

			<form action="login_traitement.php" method="POST">
				<?php
					echo "<br>";

					//Champ identifiant
					echo "Identifiant : <input type=\"text\" name=\"id\"><br>";

					//Champ mot de passe
					echo "Mot de passe : <input type=\"password\" name=\"mdp\"><br>";
				?>

				<br>
				<input type="submit" value="Se connecter" name="valider">

			</form>
			
		</center>
	</body>
</html>
	
<?php
//Script vérifiant si un utilisateur est connecté et renvoie au login s'il ne l'est pas sinon à la liste du matériel

	//Permet d'accéder aux variables de session
	session_start();

	//Renvoie au tableau de reservation si la session n'a pas été fermée
	if(!isset($_SESSION['id']))
	{
		header('Location: login.php');
	}
?>

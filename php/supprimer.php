<?php 

	require 'check.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Suppression - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>
			<h1>INVENTAIRE RT</h1>

			<?php

				$idMat = $_GET['id'];


				require '../bdd/connect.php';//Connexion à la bdd

				$requete = "SELECT id_mat, dscpt_mat, salle_mat, statut_mat FROM Materiel WHERE id_mat = $idMat";
				$reponse = $connexion->query($requete);

			?>

			<table>
				<tr><td>ID</td><td>Description</td><td>Salle</td></tr>
					<?php

						foreach ($reponse as $ligne) {
							echo "<tr><td>".$ligne['id_mat'].
								"</td><td>".$ligne['dscpt_mat'].
								"</td><td>".$ligne['salle_mat'].
								"</td></tr>\n";
						}
					?>
			</table>

			<?php

				//Supression de l'équipement dans la bdd
				$requete = "DELETE FROM Materiel
							WHERE id_mat = :idM";
				$reponse = $connexion->prepare($requete);
				$reponse->execute(['idM' => $idMat]);

				echo "Suppression terminée !";
				echo "<br>";

			?>

			<a href="dashboard.php"> Lien vers la liste </a>

		</center>
	</body>
</html>

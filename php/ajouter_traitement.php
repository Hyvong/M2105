<?php 

	require 'check.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ajouter - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>
			<h1>INVENTAIRE RT</h1>

			<?php

				//Récupère les informations du formulaire
				$catM = $_POST['categorie'];
				$dscptM = $_POST['description'];
				$statutM = $_POST['statut'];
				$salleM = $_POST['salle'];

				require '../bdd/connect.php';//Connexion à la bdd

				//Insertion du matériel dans la base
				$requete = "INSERT INTO Materiel (cat_mat, dscpt_mat, statut_mat, salle_mat)
							VALUES (:cat , :dscpt, :statut, :salle)";

				$reponse = $connexion->prepare($requete);
				$reponse->execute(array( 'cat' => $catM , 'dscpt' => $dscptM ,'statut' => $statutM ,'salle' => $salleM ));

				echo "Ajout du matériel terminé !";
				echo "<br>";

			?>

			<a href="dashboard.php"> Lien vers la liste </a>

		</center>
	</body>
</html>

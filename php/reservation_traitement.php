<?php 

	require 'check.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Réserver - Inventaire RT</title>
		<link rel="stylesheet" type="text/css" href="../css/basic.css">
	</head>
	<body>
		<center>
			<h1>INVENTAIRE RT</h1>

			<?php
				$idEtud = $_SESSION['id'];
				$idMat = $_GET['id'];

				require '../bdd/connect.php';//Connexion à la bdd

				//Récupère les informations sur le matériel à réserver
				$requete = "SELECT id_mat, dscpt_mat, salle_mat, statut_mat FROM Materiel WHERE id_mat = $idMat";
				$reponse = $connexion->query($requete);
			?>

			<!-- Affichage des infos -->
			<table>

				<tr><td>ID</td><td>Description</td><td>Salle</td><td>Statut</td></tr>

				<?php

					foreach ($reponse as $ligne)
					{
						echo "<tr><td>".$ligne['id_mat'].
							"</td><td>".$ligne['dscpt_mat'].
							"</td><td>".$ligne['salle_mat'].
							"</td><td>".$ligne['statut_mat'].
							"</td></tr>\n";
					}
				?>

			</table>

			<?php

				//Ajout d'un tuple dans Reservation
				$requete = "INSERT INTO Reservation(debut_res, id_utilisateur, id_materiel)
							VALUES(CURRENT_TIMESTAMP, :idE, :idM)";
				$reponse = $connexion->prepare($requete);
				$reponse->execute(array('idE' => $idEtud , 'idM' => $idMat));


				//Changement du statut de l'équipement
				$requete = "UPDATE Materiel
							SET statut_mat= 'Emprunté'
							WHERE id_mat = :idM";
				$reponse = $connexion->prepare($requete);
				$reponse->execute(['idM' => $idMat]);


				echo "Réservation terminée !";
				echo "<br>";
			?>

			<a href="dashboard.php"> Lien vers la liste </a>

		</center>
	</body>
</html>
